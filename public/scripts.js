$(document).ready(function() {
    $(".feature-title").click(function() {
        var target = $(this).data("target");
        var $description = $(target);
        
        if ($description.is(":visible")) {
            $description.slideUp(500);
            $(this).find(".toggle-icon").text("+");
        } else {
            $(".feature-description").slideUp(500);  // Optional: Close other descriptions
            $(".toggle-icon").text("+");  // Reset all toggle icons
            $description.slideDown(500);
            $(this).find(".toggle-icon").text("-");
        }
    });
});
